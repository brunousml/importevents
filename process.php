<?php

# Load class
    require (dirname(__FILE__)) . "/medoo.php";
$process = new processEvents($_REQUEST);

class processEvents{

    private $requests;
    private $db;

    public function __construct($requests){

        # Set and clean requests
            $this->requests = $this->cleanRequests($requests);

        # Database
            $this->db = new medoo(
                array(
                    'database_type' => 'mysql',
                    'database_name' => "aboa",
                    'server'        => "mysql.aboa.kinghost.net",
                    'username'      => "aboa",
                    'password'      => "mia52258",
                )
            );
        $this->route();
        header('Content-Type: application/json');

    }

    private function route(){

        switch ($this->requests['method']){
            case "insert":
                $this->insert();
            break;

            case "getEvents":
                $this->getEvents();
            break;

            default:
                exit(json_encode(array("Metodo incorreto")));
            break;
        }
    }

    private function insert(){
        if( strtotime($this->requests['start_time']) > strtotime("now") ){
            $this->requests['start_time'] = date("Y-m-d H:m:s", strtotime($this->requests['start_time']));
            $sql = "INSERT INTO 
                        events (id, start_time, city) 
                    VALUES 
                        ( " . $this->requests['id'] . ", '" . $this->requests['start_time'] . "', '" . $this->requests['city'] . "')
            ";

            $insert = $this->db->query($sql);
            if($insert){
                $json = array("sucess");
            }else{
                $json = array("error");
            }

            exit(json_encode($json));    
        }
    }

    private function getEvents(){
	$sql = "SELECT id, start_time FROM events WHERE (start_time > NOW() OR start_time = NOW()) ORDER BY start_time ASC;";
        $events = $this->db->query($sql)->fetchAll();
            exit(json_encode($events));
    }

    public function antiInjection($string) {
        $string = trim($string);
        $string = strip_tags($string);
        $string = addslashes($string);
        return $string;
    }

    private function cleanRequests($requests){
        if (count($requests) > 0) {
            foreach ($requests as $key => $request) {
                $requests[$key] = $this->antiInjection($request);
            }
        } else {
            # Sem parametros no request
                $this->error(1, "Nenhum parâmetro encontrado");
        }

        return $requests;
    }
}
