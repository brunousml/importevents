
    function login(){
        FB.login(function(response) {
            $('#page-1').show();
        });
    }

    function logout(){
        FB.logout(function() {
            location.reload();
        });
    }

    function showEvents(){
	$("#login").fadeOut();
        $.getJSON("process.php", {method: "getEvents"}, function(events){
            _.each(events, function(event){
                FB.api('/' + event.id + "?fields=cover,name,start_time,description", function(response) {
                    var template = _.template($("script#base_event").html(), {event:response});
                    $("div.add_events").append(template);
                });
		$("#loading").fadeOut("fast");
            });
        });
	$("#welcome, #logout, #append_events").fadeIn("slow");
    }

    $(document).ready(function(){

        // Show events
        $("a#list").click(function(e){
            $("div#success").hide();
            showEvents();
        });

        $("#logout").click(function(){
           logout();
        });
    });

    function getEvents(query) {

        var q = 'SELECT name, eid, start_time, venue FROM event WHERE eid IN ( SELECT eid FROM event_member WHERE uid = me()  ) AND start_time > now() AND privacy = "OPEN" ORDER BY start_time asc';

        // Render Data
        FB.api(
        {
            method:"fql.query",
            query: q
        }, function(response) {
            // Render events
                _.each(response, function(event){
			var form = {
                        	id: event.eid,
	                        start_time: event.start_time,
				city: event.venue.city,
        	                method: "insert"
                	    }
                    $.post( "process.php", form, function(data){});
			
                });
                showEvents();
        });
    }

    // Load the SDK asynchronously
    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '705679242787632',
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : true  // parse XFBML
        });

        // Process
        FB.Event.subscribe('auth.authResponseChange', function(response) {
            if(response.status === 'connected'){
                FB.api("/me", function(response){
                    $("span#username").html(response.first_name);         
                });
                getEvents('me/events?fields=cover,name,location,venue,start_time,admins.fields(cover),description&limit=10000');
            }else if(response.status === 'not_authorized') {

            } else {
         
            }
        });
    };

_.template.formatdate = function (stamp) {
    var d = new Date(stamp), // or d = new Date(date)
        fragments = [
            d.getDate(),
            d.getMonth() + 1,
            d.getFullYear()
        ]; 
    return fragments.join('/');
};
