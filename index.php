
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Qual a boa de hoje?</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Veja todos os eventos do facebook. Voce compartilha e ajuda a comunidade a crescer.">
    <meta name="author" content="">
    <meta name="keywords" content="eventos, facebook, todos, compartilhar, festa, show, agenda, rio">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link href="assets/css/bootplus.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 20px;
            padding-bottom: 40px;
            background-color: #FFF;
        }

        #data{display: none;}
        /* Custom container */
        .container-narrow {
            margin: 0 auto;
            max-width: 700px;
        }
        .container-narrow > hr {
            margin: 30px 0;
        }

        /* Main marketing message and sign up button */
        .jumbotron {
            margin: 60px 0;
            text-align: center;
        }
        .jumbotron h1 {
            font-size: 72px;
            line-height: 1;
        }
        .jumbotron .btn {
            font-size: 21px;
            padding: 14px 24px;
        }

        /* Supporting marketing content */
        .marketing {
            margin: 60px 0;
        }
        .marketing p + h4 {
            margin-top: 28px;
        }

        .hide{
            display: none;
        }

        #spinner {
            position: relative;
            top: 50%;
            left: 50%;
            margin: 100px 0 0 -100px;
            height: 200px;
            width: 200px;
            text-indent: 250px;
            white-space: nowrap;
            overflow: hidden;
            -webkit-mask-image: url(assets/img/spinner.png);
            background-color: #000;
            -webkit-animation-name: spinnerRotate;
            -webkit-animation-duration: 2s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-timing-function: linear;
        }

        @-webkit-keyframes spinnerRotate {
            from {
                -webkit-transform:rotate(0deg);
            }
            to {
                -webkit-transform:rotate(360deg);
            }
        }

	.item { width: 25%; }
	.item.w2 { width: 50%; }
    </style>
    <link href="assets/css/bootplus-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
</head>

<body>

    <div class="container-narrow"  >
        <div class="masthead" style="min-height:20px" >
            <ul class="nav nav-pills pull-right">
		<li id="loading"><img src="http://www.mountainchalets.com/layout/progress-bar.gif" /></li>
                <li id="logout" class="hide"><a href="#">Sair</a></li>
                <!--            <li><a href="#">About</a></li>-->
            </ul>
            <h4 id="welcome" class="hide">Qual a boa, <span id="username">$User</span>?</h4>
            <h3 class="muted"></h3>
        </div><hr>

        <div id="container" style="height: 100%; width:100%; ">


		    <div id="login"  > 
                <h2 style="font-size:50px;">Qual a boa?</h2>
                <h4>Compartilhar seus eventos com a comunidade e tenha acesso ao maior repositório de eventos da internet.</h4>         <p><fb:login-button show-faces="true" scope="user_events" width="200" max-rows="1"></fb:login-button></p>
                <span style="color:red; font-size:10px">Pra saber qual a boa de hoje é só clicar no login</span>
            </div>

            <div id="append_events" class="hide"><div class="add_events"></div></div>
            <div style="clear: both;"></div>
        </div>
    </div>

    <!--scripts-->
    <script src="assets/js/jquery.js"></script>
    <script src="http://underscorejs.org/underscore.js"></script>
    <script src="http://masonry.desandro.com/masonry.pkgd.min.js"></script>
    <script src="app.js"></script>


    <!--template-->
    <script type="text/html" id='base_event'>
	<% if(event.name){ %>
        <div class="card hovercard item" style="float: left; margin: 10px">
	            <% if(event.cover){ %>
			<a href="https://www.facebook.com/<%=event.id%>" target="_blank">
			<img src="<%=event.cover.source%>" alt=""/>
			</a>
		    <% } %>
            <div class="info">
		<h3><%= _.template.formatdate(event.start_time)%></h3>
            <div class="title">
		<a href="https://www.facebook.com/<%=event.id%>" target="_blank"><%=event.name%></a>
                </div>
            <div class="desc"><%=event.description%></div>
        </div>
        </div>
	<% } %>
    </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48771643-1', 'kinghost.net');
  ga('send', 'pageview');

</script>
</body>
</html>

